﻿using CustomerData.App_Code;
using CustomerData.Data.DataAccess;
using System;
using System.Windows.Forms;

namespace CustomerData.Areas.Customers.Forms
{
    public partial class EditCustomerForm : Form
    {
        #region VARIABELS

        private delegate void Loader();
        private long id;

        #endregion

        public EditCustomerForm(long id)
        {
            InitializeComponent();
            this.id = id;
            NumericTextBox.EnableFloat(txtMaxLimit);
        }

        private void EditCustomerForm_Shown(object sender, EventArgs e)
        {
            OnLoad();
        }

        private async void OnLoad()
        {
            Loader loading = new Loader(StartLoad);
            BeginInvoke(loading);

            try
            {
                var dataAccess = new CustomerDataAccess();
                var customer = await dataAccess.GetCustomerById(id);
                if (customer == null)
                {
                    MessageBox.Show("Customer not found"
                        , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    btnCancel.PerformClick();
                    return;
                }

                txtId.Text = customer.Id.ToString();
                txtFirstName.Text = customer.FirstName;
                txtLastName.Text = customer.LastName;
                txtMaxLimit.Text = customer.MaxLimit.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("An error occurred\nCannot load customer data"
                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                btnCancel.PerformClick();
                return;
            }

            loading = new Loader(EndLoad);
            BeginInvoke(loading);
        }

        private void StartLoad()
        {
            loader.Visible = true;
            btnSave.Enabled = false;
        }

        private void EndLoad()
        {
            loader.Visible = false;
            btnSave.Enabled = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFirstName.Text.Trim())
                || string.IsNullOrEmpty(txtLastName.Text.Trim()))
            {
                MessageBox.Show("Customer first name and last name are required"
                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);

                return;
            }

            if (MessageBox.Show("Do you want to edit this customer data ?"
                                        , ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question
                                        , MessageBoxDefaultButton.Button1) == DialogResult.No)
                return;

            double maxLimit = 0; double.TryParse(txtMaxLimit.Text, out maxLimit);
            Save(id, txtFirstName.Text.Trim(), txtLastName.Text.Trim(), maxLimit);
        }

        private async void Save(long id, string firstName, string lastName, double maxLimit)
        {
            Loader loading = new Loader(StartSave);
            BeginInvoke(loading);

            try
            {
                var dataAccess = new CustomerDataAccess();
                var result = await dataAccess.Edit(id, firstName, lastName, maxLimit);
                if (result.Done)
                    MessageBox.Show("Customer edited successfully"
                                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                else if (result.NotFound)
                {
                    MessageBox.Show("Customer not found"
                                            , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Close();
                    return;
                }
                else if (result.NameExist)
                    MessageBox.Show("Customer name already exist"
                                            , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else if (result.Error)
                    MessageBox.Show("An error occurred\nCannot edit customer data"
                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (Exception)
            {
                MessageBox.Show("An error occurred\nCannot edit customer data"
                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            loading = new Loader(EndSave);
            BeginInvoke(loading);
        }

        private void StartSave()
        {
            loader.Visible = true;
            btnSave.Enabled = btnCancel.Enabled = false;
        }

        private void EndSave()
        {
            loader.Visible = false;
            btnSave.Enabled = btnCancel.Enabled = true;
        }
    }
}
