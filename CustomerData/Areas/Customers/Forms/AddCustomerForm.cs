﻿using CustomerData.App_Code;
using CustomerData.Data.DataAccess;
using System;
using System.Windows.Forms;

namespace CustomerData.Areas.Customers.Forms
{
    public partial class AddCustomerForm : Form
    {
        #region VARIABELS

        delegate void Loader();

        #endregion

        public AddCustomerForm()
        {
            InitializeComponent();
            NumericTextBox.EnableFloat(txtMaxLimit);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFirstName.Text.Trim())
                || string.IsNullOrEmpty(txtLastName.Text.Trim()))
            {
                MessageBox.Show("Customer first name and last name are required"
                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);

                return;
            }

            if (MessageBox.Show("Do you want to add this new customer ?"
                                        , ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question
                                        , MessageBoxDefaultButton.Button1) == DialogResult.No)
                return;

            double maxLimit = 0; double.TryParse(txtMaxLimit.Text, out maxLimit);
            Save(txtFirstName.Text.Trim(), txtLastName.Text.Trim(), maxLimit);
        }

        private async void Save(string firstName, string lastName, double maxLimit)
        {
            Loader loading = new Loader(StartSave);
            BeginInvoke(loading);

            try
            {
                var dataAccess = new CustomerDataAccess();
                var result = await dataAccess.Add(firstName, lastName, maxLimit);
                if (result.Done)
                    MessageBox.Show("Customer added successfully"
                                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                else if (result.NameExist)
                    MessageBox.Show("Customer name already exist"
                                            , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else if (result.Error)
                    MessageBox.Show("An error occurred\nCannot add customer"
                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (Exception)
            {
                MessageBox.Show("An error occurred\nCannot add customer"
                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            loading = new Loader(EndSave);
            BeginInvoke(loading);
        }

        private void StartSave()
        {
            loader.Visible = true;
            btnSave.Enabled = btnCancel.Enabled = false;
        }

        private void EndSave()
        {
            loader.Visible = false;
            btnSave.Enabled = btnCancel.Enabled = true;
        }
    }
}
