﻿using CustomerData.App_Code;
using CustomerData.Areas.Reporting.Forms;
using CustomerData.Data.DataAccess;
using CustomerData.Data.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace CustomerData.Areas.Customers.Forms
{
    public partial class CustomerForm : Form
    {
        #region VARIABELS

        private delegate void Loader();
        private DataNotifier<Customer> notifier;

        #endregion

        public CustomerForm()
        {
            InitializeComponent();
            Initialize();
        }

        private void Initialize()
        {
            btnEdit.Enabled = btnDelete.Enabled = false;
            grvCustomers.EnableHeadersVisualStyles = false;

            grvCustomers.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 14);
            grvCustomers.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            grvCustomers.ColumnHeadersDefaultCellStyle.BackColor = Color.BlueViolet;
            grvCustomers.DefaultCellStyle.SelectionBackColor = Color.Lime;
            grvCustomers.DefaultCellStyle.SelectionForeColor = Color.Black;

            notifier = new DataNotifier<Customer>("Customers", "dbo");
            notifier.Listen(NotifyAdded, NotifyUpdated, NotifyDeleted);
        }

        private void CustomerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            notifier.Dispose();
        }

        private void btnViewAll_Click(object sender, EventArgs e)
        {
            ViewAll();
        }

        private void txtFirstName_TextChanged(object sender, EventArgs e)
        {
            if (loader.Visible)
                return;
            Search(txtFirstName.Text.Trim());
        }

        private async void ViewAll()
        {
            Loader loading = new Loader(StartSearch);
            BeginInvoke(loading);

            try
            {
                var dataAccess = new CustomerDataAccess();
                var customers = await dataAccess.GetAllCustomers();
                if (customers == null)
                    MessageBox.Show("Cannot search for customers"
                        , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                else
                    grvCustomers.DataSource = customers;
            }
            catch (Exception)
            {
                MessageBox.Show("An error occurred\nCannot search for customers"
                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            loading = new Loader(EndSearch);
            BeginInvoke(loading);
        }

        private async void Search(string name)
        {
            Loader loading = new Loader(StartSearch);
            BeginInvoke(loading);

            try
            {
                var dataAccess = new CustomerDataAccess();
                var customers = await dataAccess.SearchCustomers(name);
                if (customers == null)
                    MessageBox.Show("Cannot search for customers"
                        , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                else
                    grvCustomers.DataSource = customers;
            }
            catch (Exception)
            {
                MessageBox.Show("An error occurred\nCannot search for customers"
                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            loading = new Loader(EndSearch);
            BeginInvoke(loading);
        }

        private void StartSearch()
        {
            loader.Visible = true;
            btnViewAll.Enabled = btnEdit.Enabled = btnDelete.Enabled = false;
        }

        private void EndSearch()
        {
            loader.Visible = false;
            btnViewAll.Enabled = btnEdit.Enabled = btnDelete.Enabled = true;

            if (grvCustomers.RowCount == 0 || grvCustomers.CurrentRow.Index == -1)
                btnEdit.Enabled = btnDelete.Enabled = false;
            else
                btnEdit.Enabled = btnDelete.Enabled = true;
            lblResult.Text = $"({grvCustomers.Rows.Count}) customers were found";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddCustomerForm child = new AddCustomerForm();
            child.ShowDialog();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            long id = 0; long.TryParse(grvCustomers.CurrentRow.Cells[0].Value.ToString(), out id);
            EditCustomerForm child = new EditCustomerForm(id);
            child.ShowDialog();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to delete this customer data ?"
                                        , ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning
                                        , MessageBoxDefaultButton.Button1) == DialogResult.No)
                return;

            int rowIndex = grvCustomers.CurrentRow.Index;
            long id = 0; long.TryParse(grvCustomers.CurrentRow.Cells[0].Value.ToString(), out id);
            Delete(id, rowIndex);
        }

        private async void Delete(long id, int rowIndex)
        {
            Loader loading = new Loader(StartDelete);
            BeginInvoke(loading);

            try
            {
                var dataAccess = new CustomerDataAccess();
                var result = await dataAccess.Delete(id);
                if (result.Done)
                {
                    MessageBox.Show("Customer deleted successfully"
                                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (result.NotFound)
                    MessageBox.Show("Customer not found"
                                            , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else if (result.Error)
                    MessageBox.Show("An error occurred\nCannot delete customer data"
                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (Exception)
            {
                MessageBox.Show("An error occurred\nCannot edit customer data"
                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            loading = new Loader(EndDelete);
            BeginInvoke(loading);
        }

        private void StartDelete()
        {
            loader.Visible = true;
            btnViewAll.Enabled = btnEdit.Enabled = btnDelete.Enabled = false;
        }

        private void EndDelete()
        {
            loader.Visible = false;
            btnViewAll.Enabled = btnEdit.Enabled = btnDelete.Enabled = true;

            if (grvCustomers.RowCount == 0 || grvCustomers.CurrentRow.Index == -1)
                btnEdit.Enabled = btnDelete.Enabled = false;
            else
                btnEdit.Enabled = btnDelete.Enabled = true;
        }

        private void NotifyAdded(Customer customer)
        {
            List<Customer> customers = (List<Customer>)grvCustomers.DataSource;
            grvCustomers.DataSource = new List<Customer>();
            if (customers == null)
                customers = new List<Customer>();
            customers.Add(customer);
            grvCustomers.DataSource = customers;

            if (grvCustomers.RowCount == 0 || grvCustomers.CurrentRow.Index == -1)
                btnEdit.Enabled = btnDelete.Enabled = false;
            else
                btnEdit.Enabled = btnDelete.Enabled = true;
            lblResult.Text = $"({grvCustomers.Rows.Count}) customers were found";
        }

        private void NotifyUpdated(Customer customer)
        {
            grvCustomers.CurrentCell = null;
            List<Customer> customers = (List<Customer>)grvCustomers.DataSource;
            grvCustomers.DataSource = new List<Customer>();
            if (customers == null)
                customers = new List<Customer>();
            if (customers.Any(c => c.Id.Equals(customer.Id)))
            {
                var oldCustomer = customers.Single(c => c.Id.Equals(customer.Id));
                oldCustomer = customer;
            }
            grvCustomers.DataSource = customers;
        }

        private void NotifyDeleted(Customer customer)
        {
            grvCustomers.CurrentCell = null;
            List<Customer> customers = (List<Customer>)grvCustomers.DataSource;
            grvCustomers.DataSource = new List<Customer>();
            if (customers == null)
                customers = new List<Customer>();
            if (customers.Any(c => c.Id.Equals(customer.Id)))
                customers.RemoveAll(c => c.Id.Equals(customer.Id));
            grvCustomers.DataSource = customers;
            lblResult.Text = $"({grvCustomers.Rows.Count}) customers were found";
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            CustomersReportForm child = new CustomersReportForm();
            child.ShowDialog();
        }
    }
}
