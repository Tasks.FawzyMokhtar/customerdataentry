﻿using CustomerData.Areas.Customers.Forms;
using CustomerData.Data.DataAccess;
using System;
using System.Linq;
using System.Windows.Forms;

namespace CustomerData.Areas.Main.Forms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            lblTitle.Text = "Welcome!";
        }

        private async void MainForm_Load(object sender, EventArgs e)
        {
            // Initialize sample data for the first use
            btnCustomers.Enabled = false;
            CustomerDataAccess dataAccess = new CustomerDataAccess();
            await dataAccess.InitializeData();
            btnCustomers.Enabled = true;

            btnCustomers.PerformClick();
        }

        private void MainForm_MdiChildActivate(object sender, EventArgs e)
        {
            if (ActiveMdiChild != null)
            {
                lblTitle.Text = ActiveMdiChild.Text;
            }
            else
            {
                lblTitle.Text = "Welcome!";
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnCustomers_Click(object sender, EventArgs e)
        {
            if (MdiChildren.SingleOrDefault(a => a.Name == "CustomerForm") != null)
            {
                MdiChildren.Single(a => a.Name == "CustomerForm").Select();
                return;
            }

            CustomerForm child = new CustomerForm();
            child.MdiParent = this;
            child.Show();
        }
    }
}
