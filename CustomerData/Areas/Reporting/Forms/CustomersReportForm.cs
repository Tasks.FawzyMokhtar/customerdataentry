﻿using CustomerData.Data.DataAccess;
using CustomerData.Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using CustomerData.Areas.Reporting.Reports;
using CustomerData.Areas.Reporting.DataSets;

namespace CustomerData.Areas.Reporting.Forms
{
    public partial class CustomersReportForm : Form
    {
        public CustomersReportForm()
        {
            InitializeComponent();
        }

        private async void CustomersReportForm_Load(object sender, EventArgs e)
        {
            try
            {
                var customers = await GetCustomers();
                if (customers == null)
                    return;
                var ds = new CustomerDataSet();
                customers.ForEach(customer =>
                    ds.CustomerDataTable.Rows.Add(new object[] {
                    customer.Id,
                    $"{customer.FirstName} {customer.LastName}",
                    customer.MaxLimit
                    }));
                var report = new CustomersReport();
                report.SetDataSource(ds);
                rptViewer.ReportSource = report;
            }
            catch (Exception)
            {
                MessageBox.Show("An error occurred\nCannot load customers"
                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private async Task<List<Customer>> GetCustomers()
        {
            List<Customer> customers = null;
            try
            {
                var dataAccess = new CustomerDataAccess();
                customers = await dataAccess.GetAllCustomers();
                if (customers == null)
                {
                    MessageBox.Show("Cannot load customers"
                        , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("An error occurred\nCannot load customers"
                         , ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            return customers;
        }

        private void CustomersReportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            rptViewer.Dispose();
        }
    }
}
