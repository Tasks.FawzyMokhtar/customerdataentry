﻿using System.Linq;
using System.Windows.Forms;

namespace CustomerData.App_Code
{
    public static class NumericTextBox
    {
        /// <summary>
        /// Enables only integer numbers to be written in the given TextBox control 
        /// </summary>
        /// <param name="textBoxControl">The Textbox control that will be proceeded</param>
        public static TextBox EnableInteger(TextBox textBoxControl)
        {
            if (textBoxControl != null)
            {
                textBoxControl.KeyDown += (obj, args) =>
                {
                    Keys[] allowedKeys = { Keys.Back, Keys.Right, Keys.Left, Keys.Control, Keys.A ,Keys.NumPad0 ,Keys.NumPad1 ,Keys.NumPad2 ,Keys.NumPad3 ,Keys.NumPad4 ,Keys.NumPad5
                                     ,Keys.NumPad6 ,Keys.NumPad7 ,Keys.NumPad8 ,Keys.NumPad9 ,Keys.NumLock ,Keys.Decimal };

                    args.SuppressKeyPress = !allowedKeys.Contains(args.KeyCode) && (args.KeyValue < 48 || args.KeyValue > 57);
                };
            }
            return textBoxControl;
        }

        /// <summary>
        /// Enables only integer and float numbers to be written in the given TextBox control 
        /// </summary>
        /// <param name="textBoxControl">The Textbox control that will be proceeded</param>
        public static TextBox EnableFloat(TextBox textBoxControl)
        {
            if (textBoxControl != null)
            {
                textBoxControl.KeyDown += (obj, args) =>
                {
                    Keys[] allowedKeys = { Keys.Back, Keys.OemPeriod, Keys.Right, Keys.Left , Keys.Control,Keys.A
                                         ,Keys.NumPad0 ,Keys.NumPad1 ,Keys.NumPad2 ,Keys.NumPad3 ,Keys.NumPad4 ,Keys.NumPad5
                                     ,Keys.NumPad6 ,Keys.NumPad7 ,Keys.NumPad8 ,Keys.NumPad9 ,Keys.NumLock ,Keys.Decimal };

                    args.SuppressKeyPress = !allowedKeys.Contains(args.KeyCode) && (args.KeyValue < 48 || args.KeyValue > 57)
                        || (textBoxControl.Text.Contains('.') && args.KeyData == Keys.OemPeriod)
                        || (textBoxControl.Text.Contains('.') && args.KeyData == Keys.Decimal);
                };
            }
            return textBoxControl;
        }
    }
}
