﻿using CustomerData.Data.DataAccess;
using System;
using TableDependency.SqlClient;
using TableDependency.SqlClient.Base;
using TableDependency.SqlClient.Base.Enums;
using TableDependency.SqlClient.Base.EventArgs;

namespace CustomerData.App_Code
{
    public class DataNotifier<TEntity> : IDisposable
        where TEntity : class, new()
    {
        #region VARIALBES

        private SqlTableDependency<TEntity> dependency;
        private string tableName;
        private string tableSchema;
        private Action<TEntity> createHandler;
        private Action<TEntity> updateHandler;
        private Action<TEntity> deleteHandler;

        #endregion

        /// <summary>
        /// Creates new instance of DataNotifier for a specific table
        /// </summary>
        /// <param name="tableName">Table name in database</param>
        /// <param name="tableSchema">Table schema in database</param>
        public DataNotifier(string tableName, string tableSchema)
        {
            this.tableName = tableName;
            this.tableSchema = tableSchema;
            Initialize();
        }

        private void Initialize()
        {
            var mapper = new ModelToTableMapper<TEntity>();
            dependency = new SqlTableDependency<TEntity>(Singleton.db.Database.Connection.ConnectionString
                , tableName, tableSchema);
            dependency.OnChanged += dependency_OnChanged;
            dependency.OnError += dependency_OnError;
            dependency.Start();
        }

        private void dependency_OnError(object sender, ErrorEventArgs e)
        {
            throw e.Error;
        }

        private void dependency_OnChanged(object sender, RecordChangedEventArgs<TEntity> e)
        {
            if (e.ChangeType != ChangeType.None)
            {
                switch (e.ChangeType)
                {
                    case ChangeType.Insert:
                        createHandler(e.Entity);
                        break;
                    case ChangeType.Update:
                        updateHandler(e.Entity);
                        break;
                    case ChangeType.Delete:
                        deleteHandler(e.Entity);
                        break;
                }
            }
        }

        /// <summary>
        /// Register the 3 basic operations (create, update, delete) handlers
        /// </summary>
        /// <param name="createHandler">To handle create operation</param>
        /// <param name="updateHandler">To handle update operation</param>
        /// <param name="deleteHandler">To handle delete operation</param>
        public void Listen(Action<TEntity> createHandler, Action<TEntity> updateHandler
            , Action<TEntity> deleteHandler)
        {
            this.createHandler = createHandler;
            this.updateHandler = updateHandler;
            this.deleteHandler = deleteHandler;
        }

        public void Dispose()
        {
            if (dependency != null)
            {
                dependency.Dispose();
                dependency = null;
            }
        }
    }
}
