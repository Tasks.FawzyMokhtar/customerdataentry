namespace CustomerData.Data.Models
{
    using System.ComponentModel.DataAnnotations;

    public partial class Customer
    {
        public long Id { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        public double MaxLimit { get; set; }
    }
}
