namespace CustomerData.Data.Models
{
    using System.Data.Entity;

    public partial class CustomersDb : DbContext
    {
        public CustomersDb()
            : base("name=CustomersDb")
        {
        }

        public virtual DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
