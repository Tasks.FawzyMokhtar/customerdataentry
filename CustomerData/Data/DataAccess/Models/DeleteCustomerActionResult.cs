﻿namespace CustomerData.Data.DataAccess.Models
{
    public class DeleteCustomerActionResult
    {
        public bool Done { get; set; }
        public bool NotFound { get; set; }
        public bool Error { get; set; }
    }
}
