﻿namespace CustomerData.Data.DataAccess.Models
{
    public class AddCustomerActionResult
    {
        public bool Done { get; set; }
        public long Id { get; set; }
        public bool NameExist { get; set; }
        public bool Error { get; set; }
    }
}
