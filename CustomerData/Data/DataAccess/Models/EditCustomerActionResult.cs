﻿namespace CustomerData.Data.DataAccess.Models
{
    public class EditCustomerActionResult
    {
        public bool Done { get; set; }
        public bool NotFound { get; set; }
        public bool NameExist { get; set; }
        public bool Error { get; set; }
    }
}
