﻿using CustomerData.Data.Models;

namespace CustomerData.Data.DataAccess
{
    public static class Singleton
    {
        private static CustomersDb _db;
        public static CustomersDb db
        {
            get
            {
                if (_db == null)
                    _db = new CustomersDb();
                return _db;
            }
        }
    }
}
