﻿using CustomerData.Data.DataAccess.Models;
using CustomerData.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerData.Data.DataAccess
{
    public class CustomerDataAccess
    {
        public CustomerDataAccess()
        {
        }

        public async Task<AddCustomerActionResult> Add(string firstName, string lastName, double maxLimit)
        {
            var result = new AddCustomerActionResult();

            try
            {
                var customer = new Customer()
                {
                    FirstName = firstName,
                    LastName = lastName,
                    MaxLimit = maxLimit
                };

                result.NameExist = Singleton.db.Customers.Any(c => c.FirstName.Equals(customer.FirstName)
                                                         && c.LastName.Equals(customer.LastName));
                if (!result.NameExist)
                {
                    Singleton.db.Customers.Add(customer);
                    var changes = await Singleton.db.SaveChangesAsync();
                    if (changes.Equals(0))
                        result.Error = true;
                    else
                    {
                        result.Id = Singleton.db.Customers.Max(c => c.Id);
                        result.Done = true;
                    }
                }
            }
            catch (Exception)
            {
                result.Error = true;
            }
            return result;
        }

        public async Task<EditCustomerActionResult> Edit(long id, string firstName, string lastName, double maxLimit)
        {
            var result = new EditCustomerActionResult();

            try
            {
                var customer = await Task.FromResult(Singleton.db.Customers.SingleOrDefault(c => c.Id.Equals(id)));
                if (customer != null)
                {
                    result.NameExist = Singleton.db.Customers.Any(c => c.FirstName.Equals(customer.FirstName)
                                                             && c.LastName.Equals(customer.LastName)
                                                             && !c.Id.Equals(id));
                    if (!result.NameExist)
                    {
                        customer.FirstName = firstName;
                        customer.LastName = lastName;
                        customer.MaxLimit = maxLimit;
                        Singleton.db.Entry(customer).State = System.Data.Entity.EntityState.Modified;

                        var changes = await Singleton.db.SaveChangesAsync();
                        if (changes.Equals(0))
                            result.Error = true;
                        else
                            result.Done = true;
                    }
                }
                else
                    result.NotFound = true;
            }
            catch (Exception)
            {
                result.Error = true;
            }
            return result;
        }

        public async Task<DeleteCustomerActionResult> Delete(long id)
        {
            var result = new DeleteCustomerActionResult();

            try
            {
                var customer = await Task.FromResult(Singleton.db.Customers.SingleOrDefault(c => c.Id.Equals(id)));
                if (customer != null)
                {
                    Singleton.db.Customers.Remove(customer);
                    var changes = await Singleton.db.SaveChangesAsync();
                    if (changes.Equals(0))
                        result.Error = true;
                    else
                        result.Done = true;
                }
                else
                    result.NotFound = true;
            }
            catch (Exception)
            {
                result.Error = true;
            }
            return result;
        }

        public async Task<Customer> GetCustomerById(long id)
        {
            Customer customer = null;
            try
            {
                customer = await Task.FromResult(Singleton.db.Customers.SingleOrDefault(c => c.Id.Equals(id)));
            }
            catch (Exception)
            {
                customer = null;
            }
            return customer;
        }

        public async Task<List<Customer>> GetAllCustomers()
        {
            List<Customer> customers = null;
            try
            {
                customers = await Task.FromResult(Singleton.db.Customers.ToList());
            }
            catch (Exception)
            {
                customers = null;
            }
            return customers;
        }

        public async Task<List<Customer>> SearchCustomers(string name)
        {
            List<Customer> customers = null;
            try
            {
                customers = await Task.FromResult(Singleton.db.Customers.Where(c =>
                                       (c.FirstName + " " + c.LastName).Contains(name)).ToList());
            }
            catch (Exception)
            {
                customers = null;
            }
            return customers;
        }

        /// <summary>
        /// Initialize the sample data for the first time use
        /// </summary>
        public async Task<bool> InitializeData()
        {
            try
            {
                if (!Singleton.db.Customers.Any())
                {
                    var customers = new List<Customer>()
                    {
                        new Customer { FirstName = "Fawzy", LastName = "Mokhtar", MaxLimit = 1090 },
                        new Customer { FirstName = "Hany", LastName = "Ali", MaxLimit = 850 },
                        new Customer { FirstName = "Ibrahim", LastName = "Ayman", MaxLimit = 700 },
                        new Customer { FirstName = "Walid", LastName = "El-Barbary", MaxLimit = 860 },
                        new Customer { FirstName = "Anas", LastName = "Ahmed", MaxLimit = 550 },
                        new Customer { FirstName = "Khairy", LastName = "Mohamed", MaxLimit = 740 },
                        new Customer { FirstName = "Mahmoud", LastName = "Fawzy", MaxLimit = 400 },
                        new Customer { FirstName = "Islam", LastName = "Farrag", MaxLimit = 1200 },
                        new Customer { FirstName = "Kamal", LastName = "Sherif", MaxLimit = 860 },
                        new Customer { FirstName = "Khaled", LastName = "Mady", MaxLimit = 390 }
                    };

                    Singleton.db.Customers.AddRange(customers);
                    await Singleton.db.SaveChangesAsync();
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

    }
}
